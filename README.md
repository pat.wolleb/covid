# covid

## Problem Statement  

Clubbers do enter incorrect personal contact information. Hence, contact tracing is impossible in case of spreader or super-spreader events.   

**Gastro Suisse Schutzkonzepte**  

https://www.gastrosuisse.ch/de/angebot/branchenwissen/informationen-covid-19/branchen-schutzkonzept-unter-covid-19/

## Competitors

### Lunchgate

- https://www.lunchgate.ch/
- https://www.lunchgate.info/gaeste-tisch-checkin-qr-code/

**Pros:**: 

- Tracing is a free feature used for generating leads. "mindestens bis Ende 2020 kostenlos."
- Existing customer base enables to broad-roll out and quickly  
- Mentioned in 20-mins https://www.20min.ch/story/gaeste-koennen-sich-in-restaurants-freiwillig-mit-qr-codes-registrieren-796996568124

**Cons:**

- Centralised 
- Data not anonymous 
- Branding focuses on reastaurants

### Swiss Covid (Offical App)

- https://www.bag.admin.ch/bag/en/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/novel-cov/swisscovid-app-und-contact-tracing.html
- https://www.ubique.ch/blog/die-geschichte-der-swisscovid-app/
- https://ethz.ch/services/en/news-and-events/solidarity/pilot-swiss-covid-app.html

**Pros:**: 

- Broad media coverage
- Support from BAG
- Data not anonymous
- Resources
- Well-funded?
- Backed by accademia

**Cons:**

- Does not solve the problem

### Ecall

- https://sms-portal.ecall.ch/sms-tracing/

### Google Form

- QR code linking to Google Form

**Pros:**: 

- KISS
- Free
- Complies with law
- Decentralised

**Cons:**

- Data not anonymous 
- Insecure 

### Pen and paper

- QR code to Google Form 

**Pros:**: 

- KISS
- Free
- Complies with law
- Decentralised

**Cons:**

- Data not anonymous 
- Insecure
- Virus might transmit via pen or paper

### Don't Give a Fuck 

- QR code to Google Form 

**Pros:**: 

- Super KISS
- Free
- Decentralised

**Cons:**

- Complies not with law
- Contact tracing not possible

## Stakeholders

### Gewerbe Polizei

- Herr Offner, Abteilungsleiter Vollzug, 044 411 72 73, early bird 

#### Interaction Log

- 8/7/2020 Blind call to Gewerbepolizei -> recived Herr Offners contact

### Lambada Club

- Husain Sin, Son of owner I grew up with 

#### Interaction Log

- 8/7/2020 call -> Suggested https://www.lunchgate.ch/ but no interest, he would do me a favour

## Architecture TBD

PWA security article: https://love2dev.com/blog/15-minute-progressive-web-app-upgrade/

**Dummy UML**
```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```
